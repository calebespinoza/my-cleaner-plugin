import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import java.io.File;

@Mojo(name = "my-cleaner-plugin")
public class CleanerPlugin extends AbstractMojo {

    public void execute() throws MojoExecutionException {
        String target = "target";
        File index = new File(System.getProperty("user.dir"));
        for (String item : index.list()) {

            if(item.matches(target)){
                getLog().info("Directory: " + item);
                File files = new File(index + "/" + item);
                deleteDir(files);
                getLog().info("Target directory was deleted!");
            }
        }
    }

    void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                deleteDir(f);
            }
        }
        file.delete();
    }
}
